import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Employee } from '../shared/employee';
import { EmployeeService } from '../shared/employee.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  @ViewChild('form',null) form: NgForm;

  startedEditingSubscribe: Subscription;
  confirmSubscription: Subscription;

  editMode = false;
  editedItemId: number;
  editedItem: Employee;

  constructor(private employeeService: EmployeeService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.startedEditingSubscribe = this.employeeService.startedEditing.subscribe((itemId) => {
      this.editMode = true;
      this.editedItemId = itemId;
      this.editedItem = this.employeeService.Employees.find(x => x.Id == this.editedItemId);
      this.form.setValue({
        Name: this.editedItem.Name,
        Email: this.editedItem.Email,
        MobileNumber: this.editedItem.MobileNumber,
        Address: this.editedItem.Address
      })
    });

    this.confirmSubscription = this.employeeService.deleted.subscribe((data) => {
      if (data) {
        this.reset(this.form);
      }
    });

  }

  onSubmit(form: NgForm) {
    const Employee = form.value;

    if (this.editMode) {
      this.employeeService.update(this.editedItemId, Employee).subscribe((data) => {
        this.toastr.success('Update Successfull', 'Success');
        this.employeeService.setAll();
      },
        (error) => {
          this.toastr.error('Update Faild', 'Error');
        })
    } else {
      this.employeeService.add(Employee).subscribe((data) => {
        this.toastr.success('Save Successfull', 'Success');
        this.employeeService.setAll();
      },
        (error) => {
          this.toastr.error('Save Faild', 'Error');
        })
    }
    this.reset(form);
  }

  reset(form?: NgForm) {
    if (form != null && form != undefined) {
      form.reset();
    }else{
      this.form.reset();
    }
    this.editMode = false;
  }

  ngOnDestroy() {
    this.startedEditingSubscribe.unsubscribe();
    this.confirmSubscription.unsubscribe();
  }

}

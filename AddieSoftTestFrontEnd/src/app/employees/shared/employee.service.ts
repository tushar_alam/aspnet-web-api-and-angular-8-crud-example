import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { Employee } from './employee';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  startedEditing = new Subject<number>();
  listChanged = new Subject<Employee[]>();
  deleted=new Subject<boolean>();

  Employees: Employee[] = [];
  Employee: Employee;
  constructor(private http: HttpClient) { }

  add(entity: Employee): Observable<Employee> {
    return this.http.post<Employee>("http://localhost:53043/api/" + "Employees/PostEmployee", entity);
  }

  update(id: number, entity: Employee): Observable<any> {
    entity.Id=id;
    return this.http.post("http://localhost:53043/api/" + "Employees/EditEmployee", entity);
  }

  delete(id: number): Observable<Employee> {
    return this.http.delete<Employee>("http://localhost:53043/api/" + "Employees" + "/" + id);
  }

  getById(id: number): Observable<Employee> {
    return this.http.get<Employee>("http://localhost:53043/api/" + "Employees" + "/" + id);
  }
  getAll(): Observable<Employee[]> {
    return this.http.get<Employee[]>("http://localhost:53043/api/" + "Employees");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.Employee = data;
    },
      (error) => {
        this.Employee = {
          Id: null,
          Name: '',
          Email: '',
          MobileNumber:'',
          Address:''
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.Employees = data;
      this.listChanged.next(this.Employees.slice())
    },
      (error) => {
        this.Employees = [];
      })
  }
}

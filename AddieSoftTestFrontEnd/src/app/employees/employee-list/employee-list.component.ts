import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Employee } from '../shared/employee';
import { EmployeeService } from '../shared/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  listChangedSubscribe: Subscription;
  confirmSubscription: Subscription;

  employeeList: Employee[] = [];

  constructor(private employeeService: EmployeeService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.employeeService.setAll();
    this.listChangedSubscribe = this.employeeService.listChanged.subscribe((data) => { this.employeeList = data},(error) => {
        this.employeeList = [];
      });

    this.confirmSubscription = this.employeeService.deleted.subscribe((data) => {
      if (data) {
        this.employeeService.setAll();
        this.toastr.warning('Deleted Successfully', 'Success');
      }
    });
  }

  onEdit(Id: number) {
    this.employeeService.startedEditing.next(Id);
  }

  onDelete(Id: number) {
    this.employeeService.delete(Id).subscribe((data) => {
      this.employeeService.deleted.next(true);
    }, (error) => {
      this.employeeService.deleted.next(false);
    });
  }


  ngOnDestroy() {
    this.confirmSubscription.unsubscribe();
    this.listChangedSubscribe.unsubscribe();
  }

}
